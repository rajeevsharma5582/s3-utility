S3-Utility for analyzing S3 bucket data using command line tool

For Bucket data: 
python s3-bucket.py <bucket-name> <KB/MB/GB>

python s3-bucket.py pythons3-rajeev GB

{'key': 'bambooVariable.PNG', 'size': 5.227513611316681e-06, 'last_modified': '2020-03-05 17:50:06', 'storage_class': 'STANDARD'}
{'key': 'qa.txt', 'size': 3.2493844628334045e-06, 'last_modified': '2020-03-05 17:51:06', 'storage_class': 'STANDARD'}

python s3-bucket.py pythons3-rajeev KB

{'key': 'bambooVariable.PNG', 'size': 5.4814453125, 'last_modified': '2020-03-05 17:50:06', 'storage_class': 'STANDARD'}
{'key': 'qa.txt', 'size': 3.4072265625, 'last_modified': '2020-03-05 17:51:06', 'storage_class': 'STANDARD'}

python s3-bucket.py pythons3-rajeev MB

{'key': 'bambooVariable.PNG', 'size': 0.005352973937988281, 'last_modified': '2020-03-05 17:50:06', 'storage_class': 'STANDARD'}
{'key': 'qa.txt', 'size': 0.0033273696899414062, 'last_modified': '2020-03-05 17:51:06', 'storage_class': 'STANDARD'}

For Bucket Global data without pricing- 
python s3-global.py

{'name': 'python3-rajeev2', 'creation_date': '2020-03-06 23:50:55', 'number_of_files': 1, 'total_size': 1.7182073593139648, 'last_modified': '2020-03-06 23:51:47', 'location': None}
{'name': 'pythons3-rajeev', 'creation_date': '2020-03-05 17:47:32', 'number_of_files': 2, 'total_size': 0.008680343627929688, 'last_modified': '2020-03-05 17:51:06', 'location': None}
{'name': 'pythons3-rajeev2', 'creation_date': '2020-03-11 06:43:09', 'number_of_files': 0, 'total_size': 0.0, 'last_modified': '2020-03-11 06:43:09', 'location': 'us-west-1'}

For Bucket Global data with pricing- (still not working, errors encountered with storage class)
python s3-global-cost.py 
