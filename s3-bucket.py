import boto3
import sys
infolist=[]

def calc_size(value, target):
  if target == 'B':
    return value
  elif target == 'KB':
    return value / 1024
  elif target == 'MB':
    return value / 1024 / 1024
  elif target == 'GB':
    return value / 1024 / 1024 / 1024
  elif target == 'TB':
    return value / 1024 / 1024 / 1024 / 1024

def bucket_info(bucket,sizetype):
  s3 = boto3.resource('s3')
  bucket = s3.Bucket(bucket)
  for object in bucket.objects.filter(Prefix=''):
    object_info = dict()
    object_info['key'] = object.key
    object_info['size'] = calc_size(float(object.size), sizetype)
    object_info['last_modified'] = object.last_modified.strftime("%Y-%m-%d %H:%M:%S")
    object_info['storage_class'] = object.storage_class
    infolist.append(object_info)
bucketname=sys.argv[1]
size=sys.argv[2]
bucket_info(bucketname,size) 
for item in infolist:
  print(item)
