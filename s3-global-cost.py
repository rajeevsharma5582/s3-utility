import boto3
import math
from ast import literal_eval
infolist=[]

def map_storage_class_name_to_code(class_name):
  classes = {
    'One Zone - Infrequent Access': 'ONEZONE_IA',
    'Standard - Infrequent Access': 'STANDARD_IA',
    'Amazon Glacier': 'GLACIER',
    'Standard': 'STANDARD',
    'Reduced Redundancy': 'RRS'
  }
  return classes[class_name]

def get_bucket_cost(bucket, pricing_info, bucket_location):
  total_by_storage_class = dict()
  for object in bucket.objects.all():
    storage_class = object.storage_class
    if storage_class not in total_by_storage_class:
      total_by_storage_class[storage_class] = 0
    total_by_storage_class[storage_class] += object.size
  cost = 0

  for storage_class, size in total_by_storage_class.items():
    class_pricing_info = pricing_info[storage_class][bucket_location]

  for price_range in class_pricing_info:
    if size >= price_range['begin_range'] and size < price_range['end_range']:
      cost += price_range['price'] * size / (1024 * 1024 * 1024)
      break

  return cost
def map_region_name_to_code(region_name):
  regions = {
    'US East (N. Virginia)': 'us-east-1',
    'US East (Ohio)': 'us-east-2',
    'US West (N. California)': 'us-west-1',
    'US West (Oregon)': 'us-west-2',
    'Canada (Central)': 'ca-central-1',
    'EU (Frankfurt)': 'eu-central-1',
    'EU (Ireland)': 'eu-west-1',
    'EU (London)': 'eu-west-2',
    'EU (Paris)': 'eu-west-3',
    'EU (Stockholm)': 'eu-north-1',
    'Asia Pacific (Tokyo)': 'ap-northeast-1',
    'Asia Pacific (Seoul)': 'ap-northeast-2',
    'Asia Pacific (Osaka-Local)': 'ap-northeast-3',
    'Asia Pacific (Singapore)': 'ap-southeast-1',
    'Asia Pacific (Sydney)': 'ap-southeast-1',
    'Asia Pacific (Mumbai)': 'ap-south-1',
    'South America (Sao Paulo)': 'sa-east-1',
    'AWS GovCloud (US-East)': 'us-gov-east-1',
    'AWS GovCloud (US)': 'us-gov-west-1',
    'AWS GovCloud (US-West)': 'us-gov-west-1',
    'Middle East (Bahrain)': 'me-south-1',
    'Asia Pacific (Hong Kong)': 'ap-east-1'
  }
  return regions[region_name]
  
def prepare_pricing_info():
  pricing = boto3.client('pricing', 'us-east-1')
  volumeTypesResponse = pricing.get_attribute_values(
    ServiceCode='AmazonS3',
    AttributeName='volumeType'
  )
  volumeTypes = list(map(lambda x: x['Value'], volumeTypesResponse['AttributeValues']))
  volumeTypes = list(filter(lambda x: x != 'Tags', volumeTypes))

  result = dict()
  for volumeType in volumeTypes:
    price = pricing.get_products(
      ServiceCode='AmazonS3',
      Filters=[
        {
          'Type': 'TERM_MATCH',
          'Field': 'volumeType',
          'Value': volumeType
        }
      ]
    )

    storage_class = map_storage_class_name_to_code(volumeType)
    result[storage_class] = dict()
    for entry in price['PriceList']:
      entry = literal_eval(entry)
      location = entry['product']['attributes']['location']
      location_code = map_region_name_to_code(location)
      options = next(iter(entry['terms']['OnDemand'].values()))['priceDimensions']
      resulting_options = []
    for _, option in options.items():
      resulting_options.append({
      'begin_range': int(option['beginRange']),
      'end_range': math.inf if option['endRange'] == 'Inf' else int(option['endRange']),
      'price': float(option['pricePerUnit']['USD'])
    })

    result[storage_class][location_code] = resulting_options
    return result
def calc_size(value, target):
  if target == 'B':
    return value
  elif target == 'KB':
    return value / 1024
  elif target == 'MB':
    return value / 1024 / 1024
  elif target == 'GB':
    return value / 1024 / 1024 / 1024
  elif target == 'TB':
    return value / 1024 / 1024 / 1024 / 1024
def build_info():
  pricing_info = prepare_pricing_info()
  s3 = boto3.resource('s3')
  for bucket in s3.buckets.all():
    bucket_info = dict()
    bucket_info['name'] = bucket.name
    bucket_info['creation_date'] = bucket.creation_date.strftime("%Y-%m-%d %H:%M:%S")
    bucket_info['number_of_files'] = int(sum(1 for _ in bucket.objects.all()))
    bucket_info['total_size'] = float(sum(o.size for o in bucket.objects.all()))
    bucket_info['total_size'] = calc_size(bucket_info['total_size'], 'MB') 
    bucket_info['last_modified'] = max(bucket.objects.all(), key=lambda o: o.last_modified, default=None)
    if bucket_info['last_modified'] is None:
      bucket_info['last_modified'] = bucket_info['creation_date']
    else:
      bucket_info['last_modified'] = bucket_info['last_modified'].last_modified.strftime("%Y-%m-%d %H:%M:%S")

    bucket_info['location'] = s3.meta.client.get_bucket_location(Bucket=bucket.name)['LocationConstraint']
    bucket_info['cost'] = get_bucket_cost(bucket, pricing_info, bucket_info['location'])
    infolist.append(bucket_info)
build_info()
for item in infolist:
  print(item)
