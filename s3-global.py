import boto3
infolist=[]
def calc_size(value, target):
  if target == 'B':
    return value
  elif target == 'KB':
    return value / 1024
  elif target == 'MB':
    return value / 1024 / 1024
  elif target == 'GB':
    return value / 1024 / 1024 / 1024
  elif target == 'TB':
    return value / 1024 / 1024 / 1024 / 1024
def build_info():
  s3 = boto3.resource('s3')
  for bucket in s3.buckets.all():
    bucket_info = dict()
    bucket_info['name'] = bucket.name
    bucket_info['creation_date'] = bucket.creation_date.strftime("%Y-%m-%d %H:%M:%S")
    bucket_info['number_of_files'] = int(sum(1 for _ in bucket.objects.all()))
    bucket_info['total_size'] = float(sum(o.size for o in bucket.objects.all()))
    bucket_info['total_size'] = calc_size(bucket_info['total_size'], 'MB') 
    bucket_info['last_modified'] = max(bucket.objects.all(), key=lambda o: o.last_modified, default=None)
    if bucket_info['last_modified'] is None:
      bucket_info['last_modified'] = bucket_info['creation_date']
    else:
      bucket_info['last_modified'] = bucket_info['last_modified'].last_modified.strftime("%Y-%m-%d %H:%M:%S")

    bucket_info['location'] = s3.meta.client.get_bucket_location(Bucket=bucket.name)['LocationConstraint']
    infolist.append(bucket_info)
build_info()
for item in infolist:
  print(item)
